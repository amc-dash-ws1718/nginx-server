# README #

This README would normally document whatever steps are necessary to get your application up and running.
updated: april 2018

### How to setup virtual machine ###

- Download Oracle virtualbox and install it.

http://download.virtualbox.org/virtualbox/5.2.0/VirtualBox-5.2.0-118431-Win.exe

- Download ubuntu server image. from

http://releases.ubuntu.com/16.04.3/ubuntu-16.04.3-server-amd64.iso

- Run virtualbox

Click New, Then follow the wizard to add machine with Linux (ubuntu 64-bit), 1024mb ram, create new harddisk dynamically partitioned.

- Once machine is made, edit settings.

In System -> Acceleration -> Enable VT-x/AMD-v (checked)
In Storage -> Click on Empty and from cd icon on left side, seleected dowloiaded ubuntu image file.
In network -> change to Bridged Network and select the network interface with internet working.

- Now start the virtual machine and follow the installation setup. For help follow the following link

https://askubuntu.com/questions/340965/how-do-i-install-ubuntu-server-step-by-step

### What is this repository for? ###

Nginx Web server
* Version 1.13.7

### How do I get set up? ###

- Respositories in use.

git clone https://github.com/nginx/nginx

- In a Live Linux system (ubuntu 16.04 in this case), Install the following packages

sudo apt-get update && sudo apt-get install ffmpeg git build-essential libpcre3 libpcre3-dev libssl-dev

then:

- Then git clone the above repositories, and change directory to nginx directory.

- Run the following commands

sudo ./auto/configure --with-http_ssl_module
sudo make
sudo make install


- To run nginx server,

 sudo /usr/local/nginx/sbin/nginx

- To check nginx server, run the ip in browser. To check ip, run the following command in linux machine
ifconfig

- to check dash player, go to browser with ip/dash.js

- To stop nginx server

 sudo /usr/local/nginx/sbin/nginx -s stop



### Who do I talk to? ###

Mamoona Aslam
semester 2
